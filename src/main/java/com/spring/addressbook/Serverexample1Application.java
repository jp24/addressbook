package com.spring.addressbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Serverexample1Application {

	public static void main(String[] args) {
		SpringApplication.run(Serverexample1Application.class, args);
	}

}
