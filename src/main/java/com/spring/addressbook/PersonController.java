package com.spring.addressbook;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
	
	@Autowired
	private PersonRepository repository;
	
	@PostMapping("/addPerson")
	public String savePerson(@RequestBody Person person) {
		person.setId(ObjectId.get());
		this.repository.save(person);
		return "Added contact with id: " + person.getId();
	}
	
	@GetMapping("/findAllPerson")
	public List<Person> getPerson(){
		return repository.findAll();
	}
	
	@GetMapping("/findAllPerson/{id}")
	public Person getPersonById(@PathVariable ObjectId _id){
		return repository.findBy_id(_id);
	}
	
	@GetMapping("/delete/{_id}")
	public String deletePerson(@PathVariable ObjectId _id){
		repository.delete(repository.findBy_id(_id));
		return "Person deleted with id: " + _id;
	}
	
	@GetMapping("/findAllPersonByLastName/{lastName}")
	public List<Person> getPersonByLastName(@PathVariable String lastName){
		return repository.findByLastName(lastName);
	}
	
	@GetMapping("/findAllPersonByFirstName/{firstName}")
	public List<Person> getPersonByFirstName(@PathVariable String firstName){
		return repository.findByFirstName(firstName);
	}

	@GetMapping("/findAllPersonByCity/{city}")
	public List<Person> getPersonByCity(@PathVariable String city){
		return repository.findByCity(city);
	}
}

