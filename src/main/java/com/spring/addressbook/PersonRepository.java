package com.spring.addressbook;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PersonRepository extends MongoRepository<Person, String>{

	public List<Person> findByFirstName(String firstName);
	public List<Person> findByLastName(String lastName);
	Person findBy_id(ObjectId _id);

	@Query("{'address.city':?0}")
	public List<Person> findByCity(String city);
}
