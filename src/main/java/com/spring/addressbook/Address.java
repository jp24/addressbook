package com.spring.addressbook;

public class Address {

	private int telNumber;
	private String email;
	private String street;
	private String city;
	private String postalCode;
	
	public Address() {}
	
	public Address(int telNumber, String email, String street, String city, String postalCode) {
		super();
		this.telNumber = telNumber;
		this.email = email;
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}
	
	public int getTelNumber() {
		return telNumber;
	}
	public void setTelNumber(int telNumber) {
		this.telNumber = telNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
}
